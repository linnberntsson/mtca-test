import pytest
from time import sleep
from tools.devices import IFC1410, EVR, PVWAIT, PRESCALER14HZ, logger

class MLVDSTestHarness:
    def __init__(
        self,
    ):
        self.IFC1410 = IFC1410('LabS-ICS:Ctrl-AMC-242')
        self.EVR = EVR('LabS-ICS:Ctrl-EVR-142')

        # self.num_of_runs = 0
        # self.new_runs = 0
        # self.old_runs = 0

        # self.param_num_of_runs = 0

    def run(
        self,
        num_of_runs,
        pulser_width,
        pulser_frequency,
    ):
        self.num_of_runs = num_of_runs
        logger.info(f"Running MLVDS test with {num_of_runs} runs, pulser width {pulser_width} ms and pulser frequency {pulser_frequency} Hz.")

        self.EVR.DlyGen0WidthSP.pv_put(pulser_width)
        
        prescaler = (PRESCALER14HZ*14)/pulser_frequency
        self.EVR.PS0DivSP.pv_put(prescaler)

        self.EVR.DlyGen0EvtTrig0SP.pv_put(14)
        
        # configure the sequencer
        self.EVR.SoftSeq0TimestampSP.pv_put([0,1])
        self.EVR.SoftSeq0EvtCodeSP.pv_put([14,127])
        self.EVR.SoftSeq0RunModeSel.pv_put("Normal")
        self.EVR.reset_sequencer() # Do not enable yet
        self.EVR.set_backplane_pulsers(0)
        sleep(PVWAIT)

        self.old_runs = self.EVR.SoftSeq0NumOfRunsI.pv_get()
        self.new_runs = self.old_runs
        logger.debug("self.old_runs = {self.old_runs}")

        # Reset sniffer
        self.IFC1410.Enable.pv_put(0)
        self.IFC1410.PollPeriod.pv_put(0.05)
        self.IFC1410.Enable.pv_put(1)
        sleep(PVWAIT)
        
        self.EVR.SoftSeq0EnableCmd.pv_put(1) # start
        while self.new_runs - self.old_runs < num_of_runs:
            self.new_runs = self.EVR.SoftSeq0NumOfRunsI.pv_get()
        self.EVR.SoftSeq0DisableCmd.pv_put(1) # stop
        sleep(PVWAIT)
        logger.debug(f"self.new_runs = {self.new_runs})")

@pytest.fixture(scope='module', autouse=True)
def reset_to_default():
    # Will be executed before the first test
    logger.debug("Resetting IOC values BEFORE tests")
    reset_harness = MLVDSTestHarness()
    reset_harness.EVR.reset_to_default()
    sleep(PVWAIT)
    yield reset_harness
    # Will be executed after the last test
    logger.debug("Resetting IOC values AFTER tests")
    reset_harness.EVR.reset_to_default()
    sleep(PVWAIT)

@pytest.fixture(scope="class", params=pytest.num_of_runs)
def param_num_of_runs(request):
    num_of_runs = request.param
    yield num_of_runs

@pytest.fixture(scope="class", params=pytest.pulser_widths)
def param_pulser_width(request):
    pulser_width = request.param
    yield pulser_width

@pytest.fixture(scope="class", params=pytest.pulser_frequencies)
def param_pulser_frequency(request):
    pulser_frequency = request.param
    yield pulser_frequency

# Standard test fixture
@pytest.fixture(scope="class")
def harness(
    reset_to_default,
    param_num_of_runs,
    param_pulser_width,
    param_pulser_frequency,
):
    harness = MLVDSTestHarness()

    harness.run(param_num_of_runs, param_pulser_width, param_pulser_frequency)

    yield harness

    # Clean up in reset_to_default
    
class TestCountEvents:

    def test_total_num_of_runs(self,harness,):
        assert harness.num_of_runs == harness.new_runs - harness.old_runs

    def test_Tx17Cnt(self, harness,):
        '''
        Test for backplane line Tx17.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Tx17Counter = harness.IFC1410.Tx17Cnt.pv_get()
        assert Tx17Counter == harness.num_of_runs, f'{Tx17Counter} counted - expected {harness.num_of_runs}'

    def test_Tx18Cnt(self, harness,):
        '''
        Test for backplane line Tx18.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Tx18Counter = harness.IFC1410.Tx18Cnt.pv_get()
        assert Tx18Counter == harness.num_of_runs, f'{Tx18Counter} counted - expected {harness.num_of_runs}'

    def test_Tx19Cnt(self, harness,):
        '''
        Test for backplane line Tx19.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Tx19Counter = harness.IFC1410.Tx19Cnt.pv_get()
        assert Tx19Counter == harness.num_of_runs, f'{Tx19Counter} counted - expected {harness.num_of_runs}'

    def test_Tx20Cnt(self, harness,):
        '''
        Test for backplane line Tx20.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Tx20Counter = harness.IFC1410.Tx20Cnt.pv_get()
        assert Tx20Counter == harness.num_of_runs, f'{Tx20Counter} counted - expected {harness.num_of_runs}'

    def test_Rx17Cnt(self, harness,):
        '''
        Test for backplane line Rx17.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Rx17Counter = harness.IFC1410.Rx17Cnt.pv_get()
        assert Rx17Counter == harness.num_of_runs, f'{Rx17Counter} counted - expected {harness.num_of_runs}'

    def test_Rx18Cnt(self, harness,):
        '''
        Test for backplane line Rx18.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Rx18Counter = harness.IFC1410.Rx18Cnt.pv_get()
        assert Rx18Counter == harness.num_of_runs, f'{Rx18Counter} counted - expected {harness.num_of_runs}'

    def test_Rx19Cnt(self, harness,):
        '''
        Test for backplane line Rx19.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Rx19Counter = harness.IFC1410.Rx19Cnt.pv_get()
        assert Rx19Counter == harness.num_of_runs, f'{Rx19Counter} counted - expected {harness.num_of_runs}'

    def test_Rx20Cnt(self, harness,):
        '''
        Test for backplane line Rx20.
        Use EVR internal sequencer to trigger the Delay Generators (Pulsers) 
        connected to the backplane physical outputs.
        Count the triggers for every backplane line using the the IFC14x0.
        '''
        Rx20Counter = harness.IFC1410.Rx20Cnt.pv_get()
        assert Rx20Counter == harness.num_of_runs, f'{Rx20Counter} counted - expected {harness.num_of_runs}'

        # TBD: assert no errors?

        
