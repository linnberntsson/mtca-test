import pytest
from tools.devices import logger

def pytest_configure():
    pytest.num_of_runs = [
        (0),
        (1),
        (60),
        # (8400), # 10 min (14 Hz)
    ]
    pytest.pulser_widths = [
        (0.01),
        (10.0),
    ]
    pytest.pulser_frequencies = [
        (14),
    ]