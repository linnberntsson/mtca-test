import pytest
from time import sleep
from tools.devices import logger
import ansible_runner
from os import sys, path
from shutil import copyfile, rmtree


class PCIeNetworkTestHarness:
    def __init__(
        self,
        pytestconfig
    ):
        private_data_dir = path.join(pytestconfig.rootpath, 'tools', 'ans-proj')
        playbook = path.join(private_data_dir, 'project', 'playbook.yml')
        hosts = path.join(private_data_dir, 'inventory', 'hosts')

        priv_dir = str(private_data_dir)
        logger.debug(f'Preparing runner in dir {priv_dir}')
        rc = ansible_runner.config.command.CommandConfig(
            private_data_dir=private_data_dir,
            runner_mode='subprocess',
            input_fd=sys.stdin, # to enter ssh pw,
            error_fd=sys.stderr,
        )

        rc.prepare_run_command(
            executable_cmd='ansible-playbook',
            # cmdline_args=[playbook, '-k', '-K', '-i', hosts, '-l', 'test_mtca', '-t', 'show_fru'],            
            cmdline_args=[playbook, '-k', '-K', '-i', hosts, '-l', 'test_mtca', '-t', 'reboot_check_cards', '-e', f'repeat={pytest.reboot_repeat}'],            
        )

        self.runner = ansible_runner.Runner(
            rc,
        )

    def run(
        self,
        pytestconfig
    ):
        r = self.runner

        logger.info(f'Running playbook. This will reboot the CPU {pytest.reboot_repeat} times')
        r.run() # run ansible playbook

        # get stdout
        last_event = list(filter(lambda x: 'event' in x and x['event'] == 'playbook_on_stats',
                                 r.events))
        runner_id = last_event[0]['runner_ident']

        # copy file
        if pytest.report_path is not '':
            ans_log = path.join(pytestconfig.rootpath, 'tools', 'ans-proj', 'artifacts', runner_id, 'stdout')
            logger.debug(f'ans_log = {ans_log}')
            dst = f'{pytest.report_path}/ansible-MTCA-Crate-{pytest.chassi_serial}_{pytest.dateandtime}'
            logger.debug(f'destination is {dst}')
            copyfile(ans_log, f'{pytest.report_path}/ansible-MTCA-Crate-{pytest.chassi_serial}_{pytest.dateandtime}')


# Standard test fixture
@pytest.fixture(scope='class')
def harness(pytestconfig):
    '''
    '''
    capmanager = pytestconfig.pluginmanager.getplugin('capturemanager')
    capmanager.suspend_global_capture(in_=True) # to receive ssh login prompt

    harness = PCIeNetworkTestHarness(pytestconfig)

    capmanager.resume_global_capture()
    logger.debug('Running runner')

    harness.run(pytestconfig)


    logger.debug('Finished running runner')
    logger.debug('r.stats = {}'.format(harness.runner.stats))

    # Will be executed before the first test

    yield harness

    # Clean up

    # NOTE! Remove entire artifacts directory
    #   1. in order to avoid clogging up 
    #   2. because ansible-runner uses this directory to find 'r.events' - will use the last entry
    try:
        rmtree(path.join(pytestconfig.rootpath, 'tools', 'ans-proj', 'artifacts'))
    except:
        logger.warning('Could not remove tools/ans-proj/artifacts')

class TestPCIeNetwork:
    '''
    '''
    def test_pcie_network_ok(self, harness):
        '''
        This test checks the Ansible status output 'ok'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        exp_ok = 10 * pytest.reboot_repeat + 2 # TASK [Gathering Facts] + 10  tasks per reboot + 1 end task
        assert stats['ok'][pytest.cpu_hostname] == exp_ok, f"Status output 'ok' should be {exp_ok}"
        
    def test_pcie_network_changed(self, harness,):
        '''
        This test checks the Ansible status output 'ok'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        exp_changed = 3 * pytest.reboot_repeat # rebootcpu.yml + lsdevsis.yml + lsdevsis.yml
        assert stats['changed'][pytest.cpu_hostname] == exp_changed, f"Status output 'changed' should be {exp_changed}"

    def test_pcie_network_processed(self, harness,):
        '''
        This test checks the Ansible status output 'processed'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert stats['processed'][pytest.cpu_hostname] == 1, f"Status output 'processed' should be 1" # 1 host

    def test_pcie_network_skipped(self, harness,):
        '''
        This test checks the Ansible status output 'skipped'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert not stats['skipped'], f"{stats['skipped'][pytest.cpu_hostname]} skipped. See error messages."

    def test_pcie_network_unreachable(self, harness,):
        '''
        This test checks the Ansible status output 'unreachable'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert not stats['dark'], f"{stats['dark'][pytest.cpu_hostname]} unreachable. Check login credentials and see error messages"
    
    def test_pcie_network_failures(self, harness,):
        '''
        This test checks the Ansible status output 'failures'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert not stats['failures'], f"{stats['failures'][pytest.cpu_hostname]} failure(s). See error messages."
    
    def test_pcie_network_ignored(self, harness,):
        '''
        This test checks the Ansible status output 'ignored'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert not stats['ignored'], f"{stats['ignored'][pytest.cpu_hostname]} ignored. See error messages."
   
    def test_pcie_network_rescued(self, harness,):
        '''
        This test checks the Ansible status output 'rescued'. 
        Use Ansible to reboot CPU a set number of times. 
        Check that EVR and Struck cards are recognized by CPU after reboot. 
        Print Ansible output to log file. 
        '''
        stats = harness.runner.stats
        assert not stats['rescued'], f"{stats['rescued'][pytest.cpu_hostname]} rescued. See error messages."
