import pytest
from time import sleep
from tools.devices import logger, IPMIManagerCPU, IPMIManagerEVR, IPMIManagerPM, IOCSTART, IOCEXIT, PVWAIT
from run_iocsh import IOC
from os import path

class crateTestHarness:
    def __init__(
        self,
        config,
    ):
        # Default IOC
        if pytest.chassi_type == '3':
            st_cmd = path.join(config.rootpath, 'tools', 'e3-ioc-ipmitester', '3u', 'st_3u.cmd' )
        else:
            # st_cmd = path.join(config.rootpath, 'tools', 'e3-ioc-ipmitester', '9u', 'st_9u.cmd' )
            logger.warning("st_9u.cmd not added to repo yet!")

        self.IOC = IOC(st_cmd,ioc_executable='iocsh')

        self.IPMIManagerCPU = IPMIManagerCPU('LabS:Ipmi-CPU-142')
        self.IPMIManagerEVR = IPMIManagerEVR('LabS:Ipmi-EVR-142')
        self.IPMIManagerPM = IPMIManagerPM('LabS:Ipmi-PM-142')

# Standard test fixture
@pytest.fixture(scope='module') # use the same IOC for all tests
def harness(
    request,
    pytestconfig,
):
    '''
    Instantiate test harness
    '''
    # Create handle to Test Harness
    harness = crateTestHarness(pytestconfig)
    harness.IOC.start()
    logger.info(f'Starting ipmiManager EPICS IOC. This takes a while ({IOCSTART} seconds)')
    sleep(IOCSTART)
    request.harness = harness

    logger.info('IOC running. Starting test.')

    yield harness

    # Clean up
    try:
        request.harness.IOC.exit()
        sleep(IOCEXIT)
    except:
        logger.warning("Could not close IPMI Manager IOC.")

class TestCPU:
    '''
    [to print]
    - LabS:Ctrl-CPU-142:FruName
    - LabS:Ctrl-CPU-142:FruId
    - LabS:Ctrl-CPU-142:Slot
    - LabS:Ctrl-CPU-142:BManuf
    - LabS:Ctrl-CPU-142:BName
    - LabS:Ctrl-CPU-142:BSN
    - LabS:Ctrl-CPU-142:BPN

    [to check - name, min, max, PV]
    - "VCC Core", 0.25,  1.6,    LabS:Ctrl-CPU-142:Sensor2Value
    - "PS 1V0",   0.25,  1.6,    LabS:Ctrl-CPU-142:Sensor3Value
    - "PS 1V05",  0.9,   1.2,    LabS:Ctrl-CPU-142:Sensor4Value
    - "3V3 PSU",  3.002, 3.4950, LabS:Ctrl-CPU-142:Sensor5Value
    - "5V PSU",   4.588, 5.4870, LabS:Ctrl-CPU-142:Sensor6Value
    - "12V PSU",  9.5120, 14.5,  LabS:Ctrl-CPU-142:Sensor7Value
    - "Ambient",  -5, 85,        LabS:Ctrl-CPU-142:Sensor8Value
    - "CPU AMB",  -5, 93,        LabS:Ctrl-CPU-142:Sensor9Value
    - "CPU INT",  -5, 100,       LabS:Ctrl-CPU-142:Sensor10Value
    '''

    # to print to test report
    def test_FruName(self, harness,):
        '''
        Check CPU FRU name and add to test report
        '''
        pytest.CPU_FruName = harness.IPMIManagerCPU.FruName.pv_get()
        assert pytest.CPU_FruName is not None

    def test_FruId(self, harness,):
        '''
        Check CPU FRU ID and add to test report
        '''        
        pytest.CPU_FruId  = harness.IPMIManagerCPU.FruId.pv_get()
        assert pytest.CPU_FruId is not None

    def test_Slot(self, harness,):
        '''
        Check CPU slot and add to test report
        ''' 
        pytest.CPU_Slot = harness.IPMIManagerCPU.Slot.pv_get()
        assert pytest.CPU_Slot is not None

    def test_BManuf(self, harness,):
        '''
        Check CPU board manufacturer and add to test report
        ''' 
        pytest.CPU_BManuf = harness.IPMIManagerCPU.BManuf.pv_get()
        assert pytest.CPU_BManuf is not None

    def test_BName(self, harness,):
        '''
        Check CPU board name and add to test report
        ''' 
        pytest.CPU_BName = harness.IPMIManagerCPU.BName.pv_get()
        assert pytest.CPU_BName is not None

    def test_BSN(self, harness,):
        '''
        Check CPU board serial number and add to test report
        ''' 
        pytest.CPU_BSN = harness.IPMIManagerCPU.BSN.pv_get()
        assert pytest.CPU_BSN is not None

    def test_BPN(self, harness,):
        '''
        Check CPU board board PN and add to test report
        ''' 
        pytest.CPU_BPN = harness.IPMIManagerCPU.BPN.pv_get()
        assert pytest.CPU_BPN is not None

    # to check
    def test_VCCCore(self, harness,):
        '''
        Check CPU VCC Core sensor value (expected: 0.25 <= value <= 1.6)
        '''
        value = harness.IPMIManagerCPU.Sen2Value.pv_get()
        assert (0.25 <= value <= 1.6), f'0.25 <= value <= 1.6'

    def test_PS1V0(self, harness,):
        '''
        Check CPU PS1V0 sensor value (expected: 0.25 <= value <= 1.6)
        '''
        value = harness.IPMIManagerCPU.Sen3Value.pv_get()
        assert (0.25 <= value <= 1.6), f'0.25 <= value <= 1.6'
        
    def test_PS1V05(self, harness,):
        '''
        Check CPU PS 1V05 sensor value (expected: 0.9 <= value <= 1.2)
        '''
        value = harness.IPMIManagerCPU.Sen4Value.pv_get()
        assert (0.9 <= value <= 1.2), f'0.9 <= value <= 1.2'
        
    def test_3V3PSU(self, harness,):
        '''
        Check CPU 3V3 PSU sensor value (expected: 3.002 <= value <= 3.4950)
        '''
        value = harness.IPMIManagerCPU.Sen5Value.pv_get()
        assert (3.002 <= value <= 3.4950), f'3.002 <= value <= 3.4950'
        
    def test_5VPSU(self, harness,):
        '''
        Check CPU 5V PSU sensor value (expected: 4.588 <= value <= 5.4870)
        '''
        value = harness.IPMIManagerCPU.Sen6Value.pv_get()
        assert (4.588 <= value <= 5.4870), f'4.588 <= value <= 5.4870'
        
    def test_12VPSU(self, harness,):
        '''
        Check CPU 12V PSU sensor value (expected: 9.5120 <= value <= 14.5)
        '''
        value = harness.IPMIManagerCPU.Sen7Value.pv_get()
        assert (9.5120 <= value <= 14.5), f'9.5120 <= value <= 14.5'
            
    def test_Ambient(self, harness,):
        '''
        Check CPU Ambient sensor value (expected: -5 <= value <= 85)
        '''
        value = harness.IPMIManagerCPU.Sen8Value.pv_get()
        assert (-5 <= value <= 85), f'-5 <= value <= 85'
            
    def test_CPUAMB(self, harness,):
        '''
        Check CPU AMB sensor value (expected: -5 <= value <= 93)
        '''
        value = harness.IPMIManagerCPU.Sen9Value.pv_get()
        assert (-5 <= value <= 93), f'-5 <= value <= 93'
            
    def test_CPUINT(self, harness,):
        '''
        Check CPU INT sensor value (expected: -5 <= value <= 100)
        '''
        value = harness.IPMIManagerCPU.Sen10Value.pv_get()
        assert (-5 <= value <= 100), f'-5 <= value <= 100'
        
class TestEVR:
    '''
    [to print]
    - LabS:Ctrl-EVR-142:FruName
    - LabS:Ctrl-EVR-142:FruId
    - LabS:Ctrl-EVR-142:Slot
    - LabS:Ctrl-EVR-142:BManuf
    - LabS:Ctrl-EVR-142:BName
    - LabS:Ctrl-EVR-142:BSN
    - LabS:Ctrl-EVR-142:BPN

    [to check - name, min, max, PV]
    - "FPGA",   5,  60,       LabS:Ctrl-EVR-142:Sensor1Value
    - "Local",  5,  60,       LabS:Ctrl-EVR-142:Sensor2Value
    - "3V3 MP", 3.088, 3.488, LabS:Ctrl-EVR-142:Sensor4Value
    - "3V3",    3.088, 3.488, LabS:Ctrl-EVR-142:Sensor5Value
    - "12V",    11.7, 12.5,   LabS:Ctrl-EVR-142:Sensor6Value
    - "1.8V",   1.744, 1.84,  LabS:Ctrl-EVR-142:Sensor7Value
    - "1V",     0.96, 1.024,  LabS:Ctrl-EVR-142:Sensor8Value
    '''
    # to print to test report
    def test_FruName(self, harness,):
        '''
        Check EVR FRU name and add to test report
        '''
        pytest.EVR_FruName = harness.IPMIManagerEVR.FruName.pv_get()
        assert pytest.EVR_FruName is not None

    def test_FruId(self, harness,):
        '''
        Check EVR FRU ID and add to test report
        '''
        pytest.EVR_FruId  = harness.IPMIManagerEVR.FruId.pv_get()
        assert pytest.EVR_FruId is not None

    def test_Slot(self, harness,):
        '''
        Check EVR slot and add to test report
        '''
        pytest.EVR_Slot = harness.IPMIManagerEVR.Slot.pv_get()
        assert pytest.EVR_Slot is not None

    def test_BManuf(self, harness,):
        '''
        Check EVR board manufacturer and add to test report
        '''
        pytest.EVR_BManuf = harness.IPMIManagerEVR.BManuf.pv_get()
        assert pytest.EVR_BManuf is not None

    def test_BName(self, harness,):
        '''
        Check EVR board name and add to test report
        '''
        pytest.EVR_BName = harness.IPMIManagerEVR.BName.pv_get()
        assert pytest.EVR_BName is not None

    def test_BSN(self, harness,):
        '''
        Check EVR board serial number and add to test report
        '''
        pytest.EVR_BSN = harness.IPMIManagerEVR.BSN.pv_get()
        assert pytest.EVR_BSN is not None

    def test_BPN(self, harness,):
        '''
        Check EVR board board PN and add to test report
        '''
        pytest.EVR_BPN = harness.IPMIManagerEVR.BPN.pv_get()
        assert pytest.EVR_BPN is not None

    # to check
    def test_FPGA(self, harness,):
        '''
        Check EVR FPGA sensor value (expected: 5 <= value <= 60)
        '''
        value = harness.IPMIManagerEVR.Sen1Value.pv_get()
        assert (5 <= value <= 60), f'5 <= value <= 60'

    def test_Local(self, harness,):
        '''
        Check EVR Local sensor value (expected: 5 <= value <= 60)
        '''
        value = harness.IPMIManagerEVR.Sen2Value.pv_get()
        assert (5 <= value <= 60), f'5 <= value <= 60'

    def test_3V3MP(self, harness,):
        '''
        Check EVR 3V3 MP sensor value (expected: 3.088 <= value <= 3.488)
        '''
        value = harness.IPMIManagerEVR.Sen4Value.pv_get()
        assert (3.088 <= value <= 3.488), f'3.088 <= value <= 3.488'

    def test_3V3(self, harness,):
        '''
        Check EVR 3V3 sensor value (expected: 3.088 <= value <= 3.488)
        '''
        value = harness.IPMIManagerEVR.Sen5Value.pv_get()
        assert (3.088 <= value <= 3.488), f'3.088 <= value <= 3.488'

    def test_12V(self, harness,):
        '''
        Check EVR 12V sensor value (expected: 11.7 <= value <= 12.5)
        '''
        value = harness.IPMIManagerEVR.Sen6Value.pv_get()
        assert (11.7 <= value <= 12.5), f'11.7 <= value <= 12.5'

    def test_1_8V(self, harness,):
        '''
        Check EVR 1.8V sensor value (expected: 1.744 <= value <= 1.84)
        '''
        value = harness.IPMIManagerEVR.Sen7Value.pv_get()
        assert (1.744 <= value <= 1.84), f'1.744 <= value <= 1.84'

    def test_1V(self, harness,):
        '''
        Check EVR 1V sensor value (expected: 0.96 <= value <= 1.024)
        '''
        value = harness.IPMIManagerEVR.Sen8Value.pv_get()
        assert (0.96 <= value <= 1.024), f'0.96 <= value <= 1.024'

class TestPM:
    '''
    [to print]
    - LabS:Ctrl-PM-101:FruName
    - LabS:Ctrl-PM-101:BManuf
    - LabS:Ctrl-PM-101:BName
    - LabS:Ctrl-PM-101:BSN
    - LabS:Ctrl-PM-101:BPN

    [to check - name, min, max, PV]
    - "12V",  10.14,  13.8, LabS:Ctrl-PM-101:Sensor19Value
    - "MP",   3.12,   3.42, LabS:Ctrl-PM-101:Sensor23Value
    '''
    # to print to test report
    def test_FruName(self, harness,):
        '''
        Check PM FRU name and add to test report
        '''
        pytest.PM_FruName = harness.IPMIManagerPM.FruName.pv_get()
        assert pytest.PM_FruName is not None

    def test_BManuf(self, harness,):
        '''
        Check PM board manufacturer and add to test report
        '''
        pytest.PM_BManuf = harness.IPMIManagerPM.BManuf.pv_get()
        assert pytest.PM_BManuf is not None

    def test_BName(self, harness,):
        '''
        Check PM board name and add to test report
        '''
        pytest.PM_BName = harness.IPMIManagerPM.BName.pv_get()
        assert pytest.PM_BName is not None

    def test_BSN(self, harness,):
        '''
        Check PM board serial number and add to test report
        '''
        pytest.PM_BSN = harness.IPMIManagerPM.BSN.pv_get()
        assert pytest.PM_BSN is not None

    def test_BPN(self, harness,):
        '''
        Check PM board PN and add to test report
        '''
        pytest.PM_BPN = harness.IPMIManagerPM.BPN.pv_get()
        assert pytest.PM_BPN is not None

    # to check
    def test_12V(self, harness,):
        '''
        Check PM 12V sensor value (expected: 10.14 <= value <= 13.8)
        '''
        value = harness.IPMIManagerPM.Sen19Value.pv_get()
        assert (10.14 <= value <= 13.8), f'10.14 <= value <= 13.8'

    def test_MP(self, harness,):
        '''
        Check PM MP sensor value (expected: 3.12 <= value <= 3.42)
        '''
        value = harness.IPMIManagerPM.Sen23Value.pv_get()
        assert (3.12 <= value <= 3.42), f'3.12 <= value <= 3.42'
