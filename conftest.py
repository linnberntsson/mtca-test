import pytest
from py.xml import html
from datetime import datetime
from configparser import ConfigParser
from tools.devices import logger
from shutil import copyfile
from os import path, mkdir

    
@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    pytest.dateandtime = datetime.now().strftime("%Y_%m_%d-%H_%M_%S")

    parser=ConfigParser(allow_no_value=True)
    settings_file = path.join(config.rootpath, 'tools', 'settings.ini')
    parser.read(str(settings_file))

    # set up the report
    common_report_path  = parser['REPORT']['REPORT_PATH']
    if common_report_path is not '':
        if not path.exists(common_report_path):
            mkdir(common_report_path)        

        pytest.chassi_serial = parser['CHASSI']['SERIAL']
        report_folder = f"MTCA-Crate-{pytest.chassi_serial}_{pytest.dateandtime}" # dir for this specific test
        pytest.report_path  =   path.join(common_report_path, report_folder )
        if not path.exists(pytest.report_path):
            mkdir(pytest.report_path)

        config.option.htmlpath = f"{pytest.report_path}/test-report-MTCA-Crate-{pytest.chassi_serial}_{pytest.dateandtime}.html"
    else:
        pytest.report_path = ''
        
    pytest.mch_mac      = parser['MCH']['MAC']
    pytest.chassi_type  = parser['CHASSI']['TYPE']
    pytest.cpu_mac1     = parser['CPU']['MAC1']
    pytest.cpu_mac2     = parser['CPU']['MAC2']
    pytest.cpu_mac3     = parser['CPU']['MAC3']
    pytest.cpu_mac4     = parser['CPU']['MAC4']
    pytest.cpu_hostname = parser['CPU']['HOSTNAME']

    pytest.report_info  = parser['REPORT']['ADDITIONAL_INFORMATION']

    # set using ipmi manager in test_crate_status:
    pytest.CPU_FruName = ''
    pytest.CPU_FruId = ''
    pytest.CPU_Slot = ''
    pytest.CPU_BManuf = ''
    pytest.CPU_BName = ''
    pytest.CPU_BSN = ''
    pytest.CPU_BPN = ''

    pytest.EVR_FruName = ''
    pytest.EVR_FruId = ''
    pytest.EVR_Slot = ''
    pytest.EVR_BManuf = ''
    pytest.EVR_BName = ''
    pytest.EVR_BSN = ''
    pytest.EVR_BPN = ''

    pytest.PM_FruName = ''
    pytest.PM_BManuf = ''
    pytest.PM_BName = ''
    pytest.PM_BSN = ''
    pytest.PM_BPN = ''


###### TEST REPORT CONFIGURATION ######

@pytest.hookimpl(tryfirst=True)
def pytest_sessionfinish(session, exitstatus):
    # Add meta data for the test report here
    session.config._metadata['MCH MAC'] = pytest.mch_mac

    session.config._metadata['CPU FruName'] = pytest.CPU_FruName
    session.config._metadata['CPU FruId']   = pytest.CPU_FruId 
    session.config._metadata['CPU Slot']    = pytest.CPU_Slot
    session.config._metadata['CPU BManuf']  = pytest.CPU_BManuf 
    session.config._metadata['CPU BName']   = pytest.CPU_BName
    session.config._metadata['CPU BSN']     = pytest.CPU_BSN
    session.config._metadata['CPU BPN']     = pytest.CPU_BPN
    session.config._metadata['CPU MAC1']    = pytest.cpu_mac1
    session.config._metadata['CPU MAC2']    = pytest.cpu_mac2
    session.config._metadata['CPU MAC3']    = pytest.cpu_mac3
    session.config._metadata['CPU MAC4']    = pytest.cpu_mac4

    session.config._metadata['EVR FruName'] = pytest.EVR_FruName
    session.config._metadata['EVR FruId']   = pytest.EVR_FruId 
    session.config._metadata['EVR Slot']    = pytest.EVR_Slot
    session.config._metadata['EVR BManuf']  = pytest.EVR_BManuf 
    session.config._metadata['EVR BName']   = pytest.EVR_BName
    session.config._metadata['EVR BSN']     = pytest.EVR_BSN
    session.config._metadata['EVR BPN']     = pytest.EVR_BPN

    session.config._metadata['PM FruName'] = pytest.PM_FruName
    session.config._metadata['PM BManuf']  = pytest.PM_BManuf 
    session.config._metadata['PM BName']   = pytest.PM_BName
    session.config._metadata['PM BSN']     = pytest.PM_BSN
    session.config._metadata['PM BPN']     = pytest.PM_BPN

# Test report configuration
def pytest_html_results_table_header(cells):
    cells.insert(2, html.th("Description"))
    cells.pop()

def pytest_html_results_table_row(report, cells):
    cells.insert(2, html.td(getattr(report, 'description', '')))
    cells.pop()

def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([html.p(pytest.report_info)])

def pytest_html_report_title(report):
    report.title = f"Test report"

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    report.description = getattr(item.function, '__doc__', '')

def pytest_exception_interact(node, call, report):
    session = node.session
    type_ = call.excinfo.type

    if type_ is KeyError:
        pytest.exit(f"Fatal error ({type_}) - ending test run.")