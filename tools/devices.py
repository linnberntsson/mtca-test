'''
Set up handles to the PVs of MTCA test system
'''

from epics import PV
from time import sleep
import logging # for debugging
from dataclasses import dataclass

PVWAIT = 0.2 # timeout value in seconds for pvput/pvget calls

# sleep values for starting up and exiting IOC (ipmi manager)
IOCSTART = 90
IOCEXIT = 2

# prescaler values for EVR
CLOCKFREQ = 88
PRESCALER14HZ = 6289464

logger = logging.getLogger() # for debug purposes (set log_level in pytest.ini)

@dataclass
class ProcessVariable:
    hname: str # # e.g. LAB:Ctrl-AMC-242
    fname: str
    pvwait: float = PVWAIT

    def __post_init__(self):
        self.PV = PV(f'{self.hname}:{self.fname}')

    def pv_get(self) -> float:
        return self.PV.get(self.pvwait)

    def pv_put(self, value: float):
        return self.PV.put(value)

class SIS8300:
    '''
    LabS-ICS:Ctrl-AMC-142
    Struck SIS8300
    '''

    def __init__(self, hname, ):
        self.name = hname # e.g. LAB:Ctrl-AMC-142
        # Acquisition Settings
        self.Acquire                = ProcessVariable(self.name, 'Acquire')
        self.TransferArrays         = ProcessVariable(self.name, 'TransferArrays')

        self.NumSamplesR            = ProcessVariable(self.name, 'NumSamplesR')
        self.NumSamples             = ProcessVariable(self.name, 'NumSamples')
       
        self.ArrayCounterRBV        = ProcessVariable(self.name, 'ArrayCounter_RBV')
        self.ArrayCounter           = ProcessVariable(self.name, 'ArrayCounter')
        self.ArrayRateRBV           = ProcessVariable(self.name, 'ArrayRate_RBV')

        # Device Information
        self.PcieTransferTimeR      = ProcessVariable(self.name, 'PcieTransferTimeR')

        # EVR settings
        self.EvrTimestamp           = ProcessVariable(self.name, 'EvrTimestamp')
        self.BeamModeEvr            = ProcessVariable(self.name, 'BeamModeEvr')
        self.BeamDestEvr            = ProcessVariable(self.name, 'BeamDestEvr')
        self.EvrBeamState           = ProcessVariable(self.name, 'EvrBeamState')
        self.EvrLink                = ProcessVariable(self.name, 'EvrLink')
        self.EvrLinkStat            = ProcessVariable(self.name, 'EvrLinkStat')
        self.EvrLinkStat            = ProcessVariable(self.name, 'EvrLinkStat')
        self.EvrLinkSevr            = ProcessVariable(self.name, 'EvrLinkSevr')

        # Timing Settings
        self.ClockSource            = ProcessVariable(self.name, 'ClockSource')
        self.ClockDivider           = ProcessVariable(self.name, 'ClockDivider')
        self.SamplingFrequency      = ProcessVariable(self.name, 'SamplingFrequency')
        self.TriggerSource          = ProcessVariable(self.name, 'TriggerSource')
        self.TriggerExternalLine    = ProcessVariable(self.name, 'TriggerExternalLine')
        self.TriggerRepeat          = ProcessVariable(self.name, 'TriggerRepeat')
        self.TriggerRepeatR         = ProcessVariable(self.name, 'TriggerRepeatR')
        self.RtmType                = ProcessVariable(self.name, 'RtmType')
        self.IrqPoll                = ProcessVariable(self.name, 'IrqPoll')

        # Channels
        for channel in range(1, 11):
            setattr(self, f'CH{channel}Control', ProcessVariable(self.name, f'CH{channel}-Control'))

    def reset_to_default(self,):
        '''
        Reset the Struck IOC to the 'golden' configuration. 
        Do _not_ enable acquire. 
        '''
        # Acquisition Settings
        self.Acquire.pv_put(0)
        self.TransferArrays.pv_put(1)
        self.NumSamples.pv_put(5e+5)
        self.ArrayCounter.pv_put(0)

        # Timing Settings
        self.ClockSource.pv_put(0)  
        self.ClockDivider.pv_put(10)  
        self.SamplingFrequency.pv_put(1.25e+08)
        self.TriggerSource.pv_put(1) # External
        self.TriggerExternalLine.pv_put(4) # BackPlane0 
        self.TriggerRepeat.pv_put(1)
        self.RtmType.pv_put(1) # SIS8900
        self.IrqPoll.pv_put(1) # Yes 

        # Channels
        # self.CH1Control.pv_put(1)
        # self.CH2Control.pv_put(1)
        # self.CH3Control.pv_put(1)
        # self.CH4Control.pv_put(1)
        # self.CH5Control.pv_put(1)
        # self.CH6Control.pv_put(1)
        # self.CH7Control.pv_put(1)
        # self.CH8Control.pv_put(1)
        # self.CH9Control.pv_put(1)
        # self.CH10Control.pv_put(1)
        for channel in range(1, 11) :
            pv = getattr(self, f'CH{channel}Control')
            pv_put = getattr(pv, 'pv_put')
            pv_put(1)

class IFC1410:
    '''
    LabS-ICS:Ctrl-AMC-242
    IFC1410
    '''
    def __init__(self, hname, ):
        self.name = hname # e.g. LAB:Ctrl-AMC-242
        self.Enable         =   ProcessVariable(self.name, 'Enable') # 0/1 ('ENABLED'/'DISABLED')
        self.PollPeriod     =   ProcessVariable(self.name, 'PollPeriod')
        self.PollPeriodRB   =   ProcessVariable(self.name, 'PollPeriod-RB')

        self.Tx17Cnt        =   ProcessVariable(self.name, 'Tx17Cnt')
        self.Tx18Cnt        =   ProcessVariable(self.name, 'Tx18Cnt')
        self.Tx19Cnt        =   ProcessVariable(self.name, 'Tx19Cnt')
        self.Tx20Cnt        =   ProcessVariable(self.name, 'Tx20Cnt')

        self.Rx17Cnt        =   ProcessVariable(self.name, 'Rx17Cnt')
        self.Rx18Cnt        =   ProcessVariable(self.name, 'Rx18Cnt')
        self.Rx19Cnt        =   ProcessVariable(self.name, 'Rx19Cnt')
        self.Rx20Cnt        =   ProcessVariable(self.name, 'Rx20Cnt')
        
class CPU:
    '''
    LabS-ICS:Ctrl-CPU-142
    MTCA CPU (Concurrent Tech)
    '''
    def __init__(self, hname, ):
        self.name = hname
        pass

class EVR:
    '''
    LabS-ICS:Ctrl-EVR-142
    MTCA EVR
    '''
    def __init__(self, hname, ):
        self.name = hname # e.g. LAB:Ctrl-EVR-142
        # Status
        self.TimeI          = ProcessVariable(self.name, 'Time-I')
        self.LinkClkI       = ProcessVariable(self.name, 'Link-Clk-I')

        # Diagnostics
        self.EnaSel         = ProcessVariable(self.name, 'Ena-Sel') # 0/1 ('Disabled'/'Enabled')
        self.LinkSts        = ProcessVariable(self.name, 'Link-Sts')
        self.PllSts         = ProcessVariable(self.name, 'Pll-Sts')
        self.TimeValidSts   = ProcessVariable(self.name, 'Time-Valid-Sts')
        self.CntLinkTimoI   = ProcessVariable(self.name, 'Cnt-LinkTimo-I')
        self.CntHwOflwI     = ProcessVariable(self.name, 'Cnt-HwOflw-I')
        self.CntSwOflwI     = ProcessVariable(self.name, 'Cnt-SwOflw-I')
        self.CntRxErrI      = ProcessVariable(self.name, 'Cnt-RxErr-I')

        # Prescalers
        self.PS0DivSP       = ProcessVariable(self.name, 'PS-0-Div-SP')
        self.PS0RateI       = ProcessVariable(self.name, 'PS-0-Rate-I')
        self.PS1DivSP       = ProcessVariable(self.name, 'PS-1-Div-SP')
        self.PS1RateI       = ProcessVariable(self.name, 'PS-1-Rate-I')
        self.PS2DivSP       = ProcessVariable(self.name, 'PS-2-Div-SP')
        self.PS2RateI       = ProcessVariable(self.name, 'PS-2-Rate-I')


        # Delay Generators (Pulsers)
        for pulser in range(0,16):
            setattr(self, f'DlyGen{pulser}EnaSel',      ProcessVariable(self.name, f'DlyGen-{pulser}-Ena-Sel'))
            setattr(self, f'DlyGen{pulser}PolaritySel', ProcessVariable(self.name, f'DlyGen-{pulser}-Polarity-Sel')) # 0/1 ('Active High/'Active Low' = 'Normal/Invert')
            setattr(self, f'DlyGen{pulser}PrescalerSP', ProcessVariable(self.name, f'DlyGen-{pulser}-Prescaler-SP'))
            setattr(self, f'DlyGen{pulser}DelaySP',     ProcessVariable(self.name, f'DlyGen-{pulser}-Delay-SP'))
            setattr(self, f'DlyGen{pulser}WidthSP',     ProcessVariable(self.name, f'DlyGen-{pulser}-Width-SP')) # µs
            setattr(self, f'DlyGen{pulser}EvtTrig0SP',  ProcessVariable(self.name, f'DlyGen-{pulser}-Evt-Trig0-SP'))
            setattr(self, f'DlyGen{pulser}EvtTrig1SP',  ProcessVariable(self.name, f'DlyGen-{pulser}-Evt-Trig1-SP'))
            setattr(self, f'DlyGen{pulser}EvtTrig2SP',  ProcessVariable(self.name, f'DlyGen-{pulser}-Evt-Trig2-SP'))

        # Backplane Outputs
        for output in range(0, 8):    
            setattr(self, f'OutBack{output}SrcPulseSP', ProcessVariable(self.name, f'Out-Back{output}-Src-Pulse-SP'))
            setattr(self, f'OutBack{output}SrcDBusSP',  ProcessVariable(self.name, f'Out-Back{output}-Src-DBus-SP'))
            setattr(self, f'OutBack{output}SrcRB',      ProcessVariable(self.name, f'Out-Back{output}-Src-RB'))


        self.OutTCLKASrcPulseSP         =   ProcessVariable(self.name, 'Out-TCLKA-Src-Pulse-SP')
        self.OutTCLKBSrcPulseSP         =   ProcessVariable(self.name, 'Out-TCLKB-Src-Pulse-SP')
        self.OutIntSrcPulseSP           =   ProcessVariable(self.name, 'Out-Int-Src-Pulse-SP')

        self.OutTCLKASrcScaleSP         =   ProcessVariable(self.name, 'Out-TCLKA-Src-Scale-SP')
        self.OutTCLKBSrcScaleSP         =   ProcessVariable(self.name, 'Out-TCLKB-Src-Scale-SP')
        self.OutIntSrcScaleSP           =   ProcessVariable(self.name, 'Out-Int-Src-Scale-SP')

        self.OutTCLKASrcDBusSP          =   ProcessVariable(self.name, 'Out-TCLKA-Src-DBus-SP')
        self.OutTCLKBSrcDBusSP          =   ProcessVariable(self.name, 'Out-TCLKB-Src-DBus-SP')
        self.OutIntSrcDBusSP            =   ProcessVariable(self.name, 'Out-Int-Src-DBus-SP')

        self.OutTCLKASrcRB              =   ProcessVariable(self.name, 'Out-TCLKA-Src-RB')
        self.OutTCLKBSrcRB              =   ProcessVariable(self.name, 'Out-TCLKB-Src-RB')
        self.OutIntSrcRB                =   ProcessVariable(self.name, 'Out-Int-Src-RB')

        # Frontplane Outputs
        for output in range(0, 4):  
            setattr(self, f'OutFP{output}SrcPulseSP',   ProcessVariable(self.name, f'Out-FP{output}-Src-Pulse-SP'))
            setattr(self, f'OutFPUV{output}SrcPulseSP', ProcessVariable(self.name, f'Out-FPUV{output}-Src-Pulse-SP'))

            setattr(self, f'OutFP{output}SrcScaleSP',   ProcessVariable(self.name, f'Out-FP{output}-Src-Scale-SP'))
            setattr(self, f'OutFPUV{output}SrcScaleSP', ProcessVariable(self.name, f'Out-FPUV{output}-Src-Scale-SP'))

            setattr(self, f'OutFP{output}SrcDBusSP',    ProcessVariable(self.name, f'Out-FP{output}-Src-DBus-SP'))
            setattr(self, f'OutFPUV{output}SrcDBusSP',  ProcessVariable(self.name, f'Out-FPUV{output}-Src-DBus-SP'))

            setattr(self, f'OutFP{output}SrcRB',        ProcessVariable(self.name, f'Out-FP{output}-Src-RB'))
            setattr(self, f'OutFPUV{output}SrcRB',      ProcessVariable(self.name, f'Out-FPUV{output}-Src-RB'))

        # Front-panel Inputs
        self.In0LvlSel      =   ProcessVariable(self.name, 'In-0-Lvl-Sel')
        self.In1LvlSel      =   ProcessVariable(self.name, 'In-1-Lvl-Sel')

        self.In0EdgeSel     =   ProcessVariable(self.name, 'In-0-Edge-Sel')
        self.In1EdgeSel     =   ProcessVariable(self.name, 'In-1-Edge-Sel')

        self.In0TrigExtSel  =   ProcessVariable(self.name, 'In-0-Trig-Ext-Sel')
        self.In1TrigExtSel  =   ProcessVariable(self.name, 'In-1-Trig-Ext-Sel')

        self.In0CodeExtSP   =   ProcessVariable(self.name, 'In-0-Code-Ext-SP')
        self.In1CodeExtSP   =   ProcessVariable(self.name, 'In-1-Code-Ext-SP')

        self.In0TrigBackSel =   ProcessVariable(self.name, 'In-0-Trig-Back-Sel')
        self.In1TrigBackSel =   ProcessVariable(self.name, 'In-1-Trig-Back-Sel')

        self.In0CodeBackSP  =   ProcessVariable(self.name, 'In-0-Code-Back-SP')
        self.In1CodeBackSP  =   ProcessVariable(self.name, 'In-1-Code-Back-SP')

        # EXPERT PANEL

        # Sequencer
        self.SoftSeq0RunModeSel         =   ProcessVariable(self.name, 'SoftSeq-0-RunMode-Sel') # 'Normal'/'Single'
        self.SoftSeq0TrigSrc0Sel        =   ProcessVariable(self.name, 'SoftSeq-0-TrigSrc-0-Sel')
        self.SoftSeq0TrigSrc1Sel        =   ProcessVariable(self.name, 'SoftSeq-0-TrigSrc-1-Sel')
        self.SoftSeq0TrigSrc2Sel        =   ProcessVariable(self.name, 'SoftSeq-0-TrigSrc-2-Sel')
        self.SoftSeq0TsResolutionSel    =   ProcessVariable(self.name, 'SoftSeq-0-TsResolution-Sel')
        self.SoftSeq0CommitCmd          =   ProcessVariable(self.name, 'SoftSeq-0-Commit-Cmd') # 'Commit'
        self.SoftSeq0UnloadCmd          =   ProcessVariable(self.name, 'SoftSeq-0-Unload-Cmd')
        self.SoftSeq0EnableCmd          =   ProcessVariable(self.name, 'SoftSeq-0-Enable-Cmd') # 1 to enable
        self.SoftSeq0DisableCmd         =   ProcessVariable(self.name, 'SoftSeq-0-Disable-Cmd') # 1 to disable
        self.SoftSeq0LoadCmd            =   ProcessVariable(self.name, 'SoftSeq-0-Load-Cmd')

        self.SoftSeq0NumOfStartsI       =   ProcessVariable(self.name, 'SoftSeq-0-NumOfStarts-I')
        self.SoftSeq0NumOfRunsI         =   ProcessVariable(self.name, 'SoftSeq-0-NumOfRuns-I')
        self.SoftSeq0ErrorRB            =   ProcessVariable(self.name, 'SoftSeq-0-Error-RB')

        self.SoftSeq0TimestampSP        =   ProcessVariable(self.name, 'SoftSeq-0-Timestamp-SP') # eg. [0,1] (no space between values)
        self.SoftSeq0TimestampRB        =   ProcessVariable(self.name, 'SoftSeq-0-Timestamp-RB')

        self.SoftSeq0EvtCodeSP          =   ProcessVariable(self.name, 'SoftSeq-0-EvtCode-SP') # eg. [14,27] (no space between values)
    
    def reset_sequencer(self,):
        '''
        Reset the sequencer.
        Do not enable. 
        '''
        self.SoftSeq0UnloadCmd.pv_put(1)
        self.SoftSeq0LoadCmd.pv_put(1)
        self.SoftSeq0DisableCmd.pv_put(1)
        self.SoftSeq0CommitCmd.pv_put('Commit')
        sleep(PVWAIT)

    def set_backplane_pulsers(
        self, 
        pulser=int,
    ):
        '''
        Set all backplane lines to value pulser
        '''        
        self.OutBack0SrcPulseSP.pv_put(pulser)
        self.OutBack1SrcPulseSP.pv_put(pulser)
        self.OutBack2SrcPulseSP.pv_put(pulser)
        self.OutBack3SrcPulseSP.pv_put(pulser)
        self.OutBack4SrcPulseSP.pv_put(pulser)
        self.OutBack5SrcPulseSP.pv_put(pulser)
        self.OutBack6SrcPulseSP.pv_put(pulser)
        self.OutBack7SrcPulseSP.pv_put(pulser)
        self.OutTCLKASrcPulseSP.pv_put(pulser)
        self.OutTCLKBSrcPulseSP.pv_put(pulser)
        self.OutIntSrcPulseSP.pv_put(pulser)
        sleep(PVWAIT)

    def reset_to_default(self,):
        '''
        Reset EVR IOC to 'golden' configuration.
        Do not enable.
        '''
        # EXPERT PANEL
        # Sequencer
        self.SoftSeq0RunModeSel.pv_put(0) # Normal
        self.SoftSeq0TrigSrc0Sel.pv_put(0)
        self.SoftSeq0TrigSrc1Sel.pv_put(0)
        self.SoftSeq0TrigSrc2Sel.pv_put(2)
        self.SoftSeq0TsResolutionSel.pv_put(2) # µs
        self.SoftSeq0UnloadCmd.pv_put(1)
        self.SoftSeq0DisableCmd.pv_put(1)

        self.SoftSeq0TimestampSP.pv_put([0,1])
        self.SoftSeq0EvtCodeSP.pv_put([14,127])
        
        self.SoftSeq0LoadCmd.pv_put(1)
        self.SoftSeq0CommitCmd.pv_put('Commit')       

        # REGULAR PANEL
        # Prescalers
        self.PS0DivSP.pv_put(PRESCALER14HZ)
        self.DlyGen0WidthSP.pv_put(10)
        self.DlyGen0EvtTrig0SP.pv_put(14)        

        # Backplane Outputs
        self.set_backplane_pulsers(0)

        sleep(PVWAIT) 

class IPMIManagerCPU:
    '''
    MTCA CPU parameters
    LabS:Ctrl:CPU-142
    '''
    def __init__(self, hname, ):
        self.name = hname    
        # 
        
        self.FruName      =   ProcessVariable(self.name, 'FruName')
        self.FruId        =   ProcessVariable(self.name, 'FruId')
        self.Slot         =   ProcessVariable(self.name, 'Slot')
        self.BManuf       =   ProcessVariable(self.name, 'BManuf')
        self.BName        =   ProcessVariable(self.name, 'BName')
        self.BSN          =   ProcessVariable(self.name, 'BSN')
        self.BPN          =   ProcessVariable(self.name, 'BPN')

        self.Sen2Value    =   ProcessVariable(self.name, 'Sen2Value')
        self.Sen3Value    =   ProcessVariable(self.name, 'Sen3Value')
        self.Sen4Value    =   ProcessVariable(self.name, 'Sen4Value')
        self.Sen5Value    =   ProcessVariable(self.name, 'Sen5Value')
        self.Sen6Value    =   ProcessVariable(self.name, 'Sen6Value')
        self.Sen7Value    =   ProcessVariable(self.name, 'Sen7Value')
        self.Sen8Value    =   ProcessVariable(self.name, 'Sen8Value')
        self.Sen9Value    =   ProcessVariable(self.name, 'Sen9Value')
        self.Sen10Value   =   ProcessVariable(self.name, 'Sen10Value')

class IPMIManagerEVR:
    '''
    MTCA EVR parameters
    LAB:Ctrl:EVR-142
    '''
    def __init__(self, hname, ):
        self.name = hname 
        self.FruName      =   ProcessVariable(self.name, 'FruName')
        self.FruId        =   ProcessVariable(self.name, 'FruId')
        self.Slot         =   ProcessVariable(self.name, 'Slot')
        self.BManuf       =   ProcessVariable(self.name, 'BManuf')
        self.BName        =   ProcessVariable(self.name, 'BName')
        self.BSN          =   ProcessVariable(self.name, 'BSN')
        self.BPN          =   ProcessVariable(self.name, 'BPN')
    
        self.Sen1Value    =   ProcessVariable(self.name, 'Sen1Value')
        self.Sen2Value    =   ProcessVariable(self.name, 'Sen2Value')
        self.Sen4Value    =   ProcessVariable(self.name, 'Sen4Value')
        self.Sen5Value    =   ProcessVariable(self.name, 'Sen5Value')
        self.Sen6Value    =   ProcessVariable(self.name, 'Sen6Value')
        self.Sen7Value    =   ProcessVariable(self.name, 'Sen7Value')
        self.Sen8Value    =   ProcessVariable(self.name, 'Sen8Value')

class IPMIManagerPM:
    '''
    MTCA Power Module parameters
    LabS:Ctrl:PM-142
    '''
    def __init__(self, hname, ):
        self.name = hname
        self.FruName       =   ProcessVariable(self.name, 'FruName')
        self.BManuf        =   ProcessVariable(self.name, 'BManuf')
        self.BName         =   ProcessVariable(self.name, 'BName')
        self.BSN           =   ProcessVariable(self.name, 'BSN')
        self.BPN           =   ProcessVariable(self.name, 'BPN')

        self.Sen19Value    =   ProcessVariable(self.name, 'Sen19Value')
        self.Sen23Value    =   ProcessVariable(self.name, 'Sen23Value')