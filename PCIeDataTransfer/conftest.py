import pytest
from tools.devices import logger

def pytest_configure():
    pytest.run_time = [
        (10),   # always run a quick 10 second test
        (60), # 1 min
        # (3600),  # 1 hour
    ]       
    pytest.PcieTransferTimeRLowLim  = 4
    pytest.PcieTransferTimeRHighLim = 6
