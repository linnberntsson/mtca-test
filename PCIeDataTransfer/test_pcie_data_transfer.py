import pytest
from time import sleep, time
from tools.devices import EVR, SIS8300, PVWAIT, PRESCALER14HZ, logger

class PCIeTestHarness:
    def __init__(
        self,
    ):
        self.SIS8300    = SIS8300('LabS-ICS:Ctrl-AMC-142')
        self.EVR        = EVR('LabS-ICS:Ctrl-EVR-142')

        self.no_of_acquisitions = 0
        self.startArrayCounterRBV = 0
        
    def run(
        self,
        run_time,
    ):
        logger.info(f'Running PCIe data transfer test for {run_time} seconds.')
        
        # Configure the EVR triggers
        self.EVR.DlyGen0WidthSP.pv_put(10) 
        self.EVR.PS0DivSP.pv_put(PRESCALER14HZ) 
        self.EVR.DlyGen0EvtTrig0SP.pv_put(14)
        self.EVR.SoftSeq0TimestampSP.pv_put([0,1])
        self.EVR.SoftSeq0EvtCodeSP.pv_put([14,127])
        self.EVR.SoftSeq0RunModeSel.pv_put('Normal')
        self.EVR.reset_sequencer() # Do not enable yet
        self.EVR.set_backplane_pulsers(0)
        self.EVR.SoftSeq0EnableCmd.pv_put(1) # start
        sleep(PVWAIT)

        # Set to no_of_acquisitions
        trigger_freq = self.EVR.PS0RateI.pv_get() * 10**6# trigger frequency [Hz]
        self.no_of_acquisitions = round(trigger_freq * run_time)
        self.SIS8300.TriggerRepeat.pv_put(self.no_of_acquisitions)
        sleep(PVWAIT)


        # Set-up to calculate average transfer time
        self.SIS8300.ArrayCounter.pv_put(0) # reset
        sleep(PVWAIT)
        self.startArrayCounterRBV = self.SIS8300.ArrayCounterRBV.pv_get() # should be = 0
        oldArrayCounterRBV = self.startArrayCounterRBV
        avg = 0
        n = 0

        # Monitor PCIe transfer time for spikes
        logger.info(f'Starting Acquire ({run_time} seconds)')
        t_end = time() + run_time + 2 # add two extra seconds
        self.SIS8300.Acquire.pv_put(1) # start acquiring

        while time() < t_end:
            newArrayCounterRBV = self.SIS8300.ArrayCounterRBV.pv_get()
            if newArrayCounterRBV > oldArrayCounterRBV:
                transfer_time_OK = True
                PcieTransferTimeR = self.SIS8300.PcieTransferTimeR.pv_get()
                
                if (PcieTransferTimeR < pytest.PcieTransferTimeRLowLim) or (PcieTransferTimeR > pytest.PcieTransferTimeRHighLim):
                    logger.warning(f'PcieTransferTimeR not normal! {PcieTransferTimeR} ms on {newArrayCounterRBV}th acquistion')
                oldArrayCounterRBV = newArrayCounterRBV

        sleep(20)
        logger.info(f'Trigger repeat = {self.SIS8300.TriggerRepeatR.pv_get()}')
        logger.info(f'No. of acquisitions (RBV) = {self.SIS8300.ArrayCounterRBV.pv_get()}')


@pytest.fixture(scope='class', params=pytest.run_time)
def param_run_time(request):
    run_time = request.param
    yield run_time

# Standard test fixture
@pytest.fixture(scope='function')
def harness(
    param_run_time,
):
    harness = PCIeTestHarness()

    # Will be executed before the first test
    logger.debug('Resetting IOC values BEFORE tests')
    harness = PCIeTestHarness()
    # set EVR IOC to 'golden config'
    harness.EVR.SoftSeq0RunModeSel.pv_put(0) # Normal
    harness.EVR.SoftSeq0TrigSrc0Sel.pv_put(0)
    harness.EVR.SoftSeq0TrigSrc1Sel.pv_put(0)
    harness.EVR.SoftSeq0TrigSrc2Sel.pv_put(2)
    harness.EVR.SoftSeq0TsResolutionSel.pv_put(2) # µs
    harness.EVR.SoftSeq0UnloadCmd.pv_put(1)
    harness.EVR.SoftSeq0DisableCmd.pv_put(1)
    harness.EVR.SoftSeq0TimestampSP.pv_put([0,1])
    harness.EVR.SoftSeq0EvtCodeSP.pv_put([14,127])
    harness.EVR.SoftSeq0LoadCmd.pv_put(1)
    harness.EVR.SoftSeq0CommitCmd.pv_put('Commit')       
    harness.EVR.PS0DivSP.pv_put(PRESCALER14HZ)
    harness.EVR.DlyGen0WidthSP.pv_put(10)
    harness.EVR.DlyGen0EvtTrig0SP.pv_put(14)        
    harness.EVR.set_backplane_pulsers(0)
    sleep(PVWAIT) 

    # set Struck IOC to 'golden config'
    harness.SIS8300.Acquire.pv_put(0)
    harness.SIS8300.TransferArrays.pv_put(1)
    harness.SIS8300.NumSamples.pv_put(5e+5)
    harness.SIS8300.ArrayCounter.pv_put(0)
    harness.SIS8300.ClockSource.pv_put(0)  
    harness.SIS8300.ClockDivider.pv_put(10)  
    harness.SIS8300.SamplingFrequency.pv_put(1.25e+08)
    harness.SIS8300.TriggerSource.pv_put(1) # External
    harness.SIS8300.TriggerExternalLine.pv_put(4) # BackPlane0 
    harness.SIS8300.TriggerRepeat.pv_put(1)
    harness.SIS8300.RtmType.pv_put(1) # SIS8900
    harness.SIS8300.IrqPoll.pv_put(1) # Yes 
    harness.SIS8300.CH1Control.pv_put(1)
    sleep(PVWAIT)

    harness.run(param_run_time)

    yield harness

    # Will be executed after the last test


class TestPCIeDataTransfer:

    def test_pcie_data_transfer(
        self,
        harness,
    ):
        '''
        Use Struck SIS8300 to promote DMA transfers between the digitizer and the MTCA CPU. 
        For every new acquisition, a new DMA transfer is performed between SIS8300 and MTCA CPU. 
        Start a periodic acquisition at 14 Hz for a set period of time and check for any missed PCIe transfer.
        Log warning if PCI transfer time exceeds specified limits.
        '''
        acquired = harness.SIS8300.ArrayCounterRBV.pv_get() - harness.startArrayCounterRBV
        assert  acquired == harness.no_of_acquisitions, f'SIS8300 acquired {acquired} - expected {harness.no_of_acquisitions}'