# mTCA System Tests
This directory contains test scripts for MTCA system tests. 

Every new MTCA crate prepared by ICSHWI-WP4 is composed of:

* Chassis structure and backplane (3U or 9U)
* Cooling Units (CUs) and Power Modules (PMs)
* CPU
* mTCA Carrier Hub - (MCH)
* MRF Event Receiver (EVR)

The crate preparation process includes the update and configuration of the MCH, installation of the CPU and EVR cards and basic setup of the CPU (Linux installation). The system tests in this directory requires additional AMCs to be installed:
* SIS8300
* IFC14x0 
These AMCs are re-used for every new system test.

## Prerequisites
In order to run the test suite, you must install the following:

 * python3

On CentOS 7, run the following:

```
sudo yum install -y python3
```

And the following python modules:

 * pytest
 * pytest-html
 * pyepics
 * ansible-runner
 * dataclasses
 * run-iocsh

You can use the following pip3 commands:

```
pip3 install -r requirements.txt
pip3 install run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
```

Also install the mtca-test package (this directory):

```
cd mtca-test
pip3 install .
```

You must configure the EPICS environment before running the test suite. 
For the E3 environment, this requires you to ``source setE3Env.bash``.

## Test configuration
In order to run the test suite with the specific CCPU and MCH you must configure the following files:

* tools/settings.ini
* tools/ans-proj/inventory/hosts
* tools/e3-ioc-ipmitester/st_3u.cmd *or* tools/e3-ioc-ipmitester/st_9u.cmd (3U or 9U mTCA crate)

**tools/settings.ini**

e.g.:
```
[CHASSI]
SERIAL=123456
; 3U or 9U
TYPE=3
[CPU]
HOSTNAME=cslab-ccpu-tester-02.cslab.esss.lu.se
MAC1=00:11:22:33:44:55
MAC2=66:77:88:99:00:11
MAC3=22:33:44:55:66:77
MAC4=88:99:00:11:22:33
[MCH]
MAC=44:55:66:77:88:99
[REPORT]
ADDITIONAL_INFORMATION=Full test on test crate
REPORT_PATH=/opt/devspace/mtca-test/reports
```

Note! The following are used in the actual test suite:
* `[CHASSI][TYPE]` 
* `[CPU][HOSTNAME]`

The following are used for setting up the report:
* `[CHASSI][SERIAL]`
* `[REPORT][REPORT_PATH]` This is the common report path - for each individual test run a new test report directory is added here with a name including date, time and crate serial number. 

The remaining entries in settings.ini are values to be included in the test report (could be empty fields). 

**tools/ans-proj/inventory/hosts**

The directory 'ans-proj' is used to run PCIeNetwork/test_pcie_network. 
In *hosts*, set the host name of the CPU and the MCH, specific for the crate to be tested.

e.g:
```
[test_mtca]
cslab-ccpu-tester-02.cslab.esss.lu.se    mch_host=cslab-mch-tester.cslab.esss.lu.se
```

**tools/e3-ioc-ipmitester/st_Xu.cmd**

The directory 'e3-ioc-ipmitester' is used to run CrateStatus/test_crate_status.py.

In st_Xu.cmd (3U or 9U), set the MCH IP address.

e.g.:
```
epicsEnvSet("MCH_ADDR", "172.30.6.67")
```

## Running the test suite
To run the entire test suite it is recommended to specify the order of test execution.  
This is simply because *test_pcie_network.py* requires SSH login details to the CPU,  
and it is easier to type these in at the start of the test.

e.g.:
```
pytest -v PCIeNetwork CrateStatus MLVDSLines PCIeDataTransfer
```
*or*
```
./run_all_mtca_tests.sh
```

To run an individual test:
```
pytest -v CrateStatus
```

## Test report
When running a test, a test report folder is added to the common report path (specified in settings.ini).  
The test report folder is named as *MTCA-Crate-SN_year_month_day-hh_mm_ss*.  
Running the tests generates a html test report which is added to the test report folder.  
The html test report can be inspected by opening it in any web browser. 
In addition, when running the tests in PCIeDataNetwork the output from the ansible script is directed to  a text file in the test report folder (also included in the html report).


## Configuring the tests

If needed, the tests can be configured further in the conftest.py of each test directory. 

**MLVDSLines/conftest.py**

Set the combination of triggers from the EVR. 

The following configuration will yield the test 6 times. 

```
pytest.num_of_runs = [
    (0),
    (1),
    (8400), # 10 min (14 Hz)
]
pytest.pulser_widths = [
    (0.01),
    (10.0),
]
pytest.pulser_frequencies = [
    (14),
]
```

**PCIeDataTransfer/conftest.py**

Set how long the test should run. 

The following configuration will yield the test 2 times.   
It will run once for 10 seconds and once for 1 hour. 
```
pytest.run_time = [
    (10),   # always run a quick 10 second test
    (3600),  # 1 hour
] 
```

**PCIeNetwork/conftest.py**

Set how many times the CPU should be rebooted. 

```
pytest.reboot_repeat    =   10
```

## Test suite ##

The test suite consists of four test routines:

### PCIeNetwork/test_pcie_network.py

The standard PCIe topology for every crate at ESS is to define the MTCA CPU (Concurrent Tech. CPUs) as the root complex and then any other AMC in the backplane as a PCIe endpoint. This is defined by the "virtual switch" configuration on the MCH. 
This test routine reboots the CPU and then verifies that the EVR and the SIS8300 cards are still detected. 

https://gitlab.esss.lu.se/julianomurari/ics-ans-role-mtca

### CrateStatus/test_crate_status.py

For this test routine, an IOC based on the e3-ipmimanager module is launched to monitor the MTCA crate from its MCH via IPMI protocol. A number of sensor values are validated and parameters such as FRU names, slot numbers etc. are added to the test report.

### MLVDSLines/test_mlvds_lines.py

This test routine aims to test the MLVDS lines (backplane lines) that are mainly used as acquisition triggers for the digitizers of the MTCA system. The routine uses the EVR and the IFC14x0 EPICS IOCs (PVs) to coordinate trigger generation and detection.

On the EVR side, triggers are created by manipulating the Delay Generators (Pulsers) connected to the backplane physical outputs, driven by events that can be generated using the EVR internal sequencers.

On the IFC14x0, the card acts as a simple "trigger" counter for every backplane line. The values of the counters are immediately available as EPICS PVs.

After running the trigger generation/acquisition for a set time, the counter for each backplane line is checked and verified. 

### PCIeDataTransfer/test_pcie_data_transfer.py

In this test routine, the EPICS IOC of the Struck SIS8300 is used to promote DMA transfers between the digitizer and the MTCA CPU. 

The data acquisition streams is commanded by the backplane triggers. For every new acquisition, a new DMA transfer is performed between SIS8300 and MTCA CPU. 

The EPICS IOC measures the PCI transfer time and increments a counter. The test routine starts a periodic acquisition at 14 Hz for a set period of time and check for any missed PCIe transfer at the end. The transfer time for each acquistion is checked and a warning is printed if the time exceeds the acceptable limits (set in PCIeDataTransfer/conftest.py).